#include <at89x51.h>		// Definitions of registers, SFRs and Bits
#include <stdio.h>

extern void uart_init(void);
extern void uart_putc(char);

// ISR Prototypes ===================================================
void INT0_ISR (void) __interrupt 0;			// ISR for External __interrupt 0
void T0_ISR (void) __interrupt 1;	// ISR for Timer0/Counter0 Overflow
void INT1_ISR (void) __interrupt 2;	// ISR for External __interrupt 1
void T1_ISR (void) __interrupt 3;	// ISR for Timer1/Counter1 Overflow
void UART_ISR (void) __interrupt 4;	// ISR for UART __interrupt

__xdata int fib[10]; // 80C31

// int fib[10]; // 89C51

void main (void)
{
	int i;
	
	int prev=0,curr=1,term;
	
	uart_init();
	
	printf_tiny("System up ...\r\n");
	
	printf_tiny("Calculating ...\r\n");
	
	for(i=0; i<10; i++)
	{
		term = prev+curr;
		prev = curr;
		curr = term;
		
		fib[i] = curr;
	}
   	
   	printf_tiny("Fibonacci series ready ...\r\n");
   	
	for(i = 0; i < 10; i++)
	{
		printf_tiny("%d ... %d\r\n", i, fib[i]);
	}
	
	printf_tiny("Done.\r\n");
	
	while(1)
	{
		
	}
}

void putchar(char c)
{
	uart_putc(c);
}

void INT0_ISR (void) __interrupt 0
{
	
}

void T0_ISR (void) __interrupt 1
{
	
}

void INT1_ISR (void) __interrupt 2
{
	
}

void T1_ISR (void) __interrupt 3
{
	
}

void UART_ISR (void) __interrupt 4
{
	
}
